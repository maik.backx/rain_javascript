<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="style.css">
  <title>Mist-animation</title>
</head>
<body>
  <script src="three.min.js"></script>
  <script type="text/javascript">
  let scene,camera, renderer, cloudParticles = [], flash;

  function init() {
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(60,window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.z = 1;
    camera.rotation.x = 1.16;
    camera.rotation.y = -0.12;
    camera.rotation.z = 0.27;

    ambient = new THREE.AmbientLight(0x555555); //ambient-lightning
    scene.add(ambient);

    directionalLight = new THREE.DirectionalLight(0xffeedd);
    directionalLight.position.set(0,0,1);
    scene.add(directionalLight);

    flash = new THREE.PointLight(0x062d89, 30, 500 ,1.7);
    flash.position.set(200,300,100);
    scene.add(flash);

    renderer = new THREE.WebGLRenderer();
    scene.fog = new THREE.FogExp2(0x11111f, 0.002); //0.002 is de dichtheid van de wolken
    renderer.setClearColor(scene.fog.color);
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    let loader = new THREE.TextureLoader();
    loader.load("cloud.png", function(texture){
      cloudGeo = new THREE.PlaneBufferGeometry(500,500); //Grootte van de wolken
      cloudMaterial = new THREE.MeshLambertMaterial({
        map: texture,
        transparent: true
      });
      for(let p=0; p<25; p++) {
        let cloud = new THREE.Mesh(cloudGeo,cloudMaterial);
        cloud.position.set(
          Math.random()*800 -400,
          500,
          Math.random()*500 - 450
        );
        cloud.rotation.x = 1.16;
        cloud.rotation.y = -0.12;
        cloud.rotation.z = Math.random()*360;
        cloud.material.opacity = 0.6;
        cloudParticles.push(cloud);
        scene.add(cloud);
      }
      animate();
    });
  }
  function animate() {
    cloudParticles.forEach(p => {
      p.rotation.z -=0.002;
    });

    //Willekeurige lichtflitsen in de wolken.
    if(Math.random() > 0.93 || flash.power > 100) {
      if(flash.power < 100) 
        flash.position.set(
          Math.random()*400,
          300 + Math.random() *200,
          100
        );
      flash.power = 50 + Math.random() * 500;
    }

    renderer.render(scene, camera);
    requestAnimationFrame(animate);
  }
  init();
  </script>
</body>
</html>
